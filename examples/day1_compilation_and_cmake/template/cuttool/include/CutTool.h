#ifndef CUTTOOL_H
#define CUTTOOL_H
#include<iostream>
#include<string>

class CutTool{
public:
	CutTool(){}
	~CutTool(){}
	
	static bool passCut(const int& tag_track_No, 
						const int& rec_track_No,
						const double& tag_track_P,
						const double& rec_track_P,
						const double& ecal_E,
						const double& hcal_E,
						const double& hcal_MaxCell);
public:
    static bool used;
};
#endif
