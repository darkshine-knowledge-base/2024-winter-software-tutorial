#include "CutTool.h"

int main(int argc,char **argv){
	if(argc!=8){
		std::cout<<"Usage: \n";
		std::cout<<"./CutTool [tag_track_No] [rec_track_No] \n";
		std::cout<<"[tag_track_P] [rec_track_P]\n";
		std::cout<<"[ecal_E] [hcal_E] [hcal_MaxCell]\n";
		std::cout<<"All energy unit should be in MeV! "<<std::endl;
		return 0;
	}
	int tag_track_No = std::stoi(std::string(argv[1]));
	int rec_track_No = std::stoi(std::string(argv[2]));
	double tag_track_P = std::stod(std::string(argv[3]));
	double rec_track_P = std::stod(std::string(argv[4]));
	double ecal_E = std::stod(std::string(argv[5]));
	double hcal_E = std::stod(std::string(argv[6]));
	double hcal_MaxCell = std::stod(std::string(argv[7]));
	bool pass = CutTool::passCut(tag_track_No,rec_track_No,
								tag_track_P,rec_track_P,
								ecal_E,hcal_E,hcal_MaxCell);
	if(pass){
		std::cout<<"Pass selection!"<<std::endl;
	}
	else{
		std::cout<<"Fail selection!"<<std::endl;
	}
	return 0;
}
