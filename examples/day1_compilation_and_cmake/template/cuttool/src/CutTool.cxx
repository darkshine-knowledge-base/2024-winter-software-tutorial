#include "CutTool.h"

bool CutTool::used = false;
bool CutTool::passCut(const int& tag_track_No, 
						const int& rec_track_No,
						const double& tag_track_P,
						const double& rec_track_P,
						const double& ecal_E,
						const double& hcal_E,
						const double& hcal_MaxCell){
    if(!used){
        std::cout<<"Welcome to Zhen & Xuliang's cut tool"<<std::endl;
        used=true;
    }
	bool pass = (tag_track_No==1) && (rec_track_No==1) &&
				((tag_track_P-rec_track_P)>4000.) &&
				(ecal_E<2500) &&
				(hcal_E<10.) &&
				(hcal_MaxCell<0.2);
	return pass;
}
