//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jan 18 08:03:07 2024 by ROOT version 6.26/14
// from TTree dp/dp
// found on file: dp_ana.root
//////////////////////////////////////////////////////////

#ifndef dp_h
#define dp_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TVector3.h>

// Header file for the classes stored in the TTree if any.
#include "TString.h"
#include "vector"

using namespace std;

class dp {
    public :
        TTree          *fChain;   //!pointer to the analyzed TTree or TChain
        Int_t           fCurrent; //!current Tree number in a TChain

        // Fixed size dimensions of array or collections stored in the TTree if any.

        // Declaration of leaf types
        Int_t           RunNumber;
        Int_t           EventNumber;
        Double_t        Rndm[4];
        Double_t        weight;
        Double_t        ECAL_Leak;
        Double_t        PNEnergy_Target;
        Double_t        PNEnergy_ECAL;
        Double_t        ENEnergy_Target;
        Double_t        ENEnergy_ECAL;
        Double_t        PNZ_Target;
        Double_t        PNZ_ECAL;
        Double_t        ENZ_Target;
        Double_t        ENZ_ECAL;
        Int_t           Digitized_Signal_No;
        Int_t           Digitized_Signal_NoGen;
        Double_t        Initial_Px;
        Double_t        Initial_Py;
        Double_t        Initial_Pz;
        Double_t        Initial_X;
        Double_t        Initial_Y;
        Double_t        Initial_Z;
        Double_t        Recoil_E;
        Double_t        Recoil_P[3];
        Double_t        Recoil_pT;
        Double_t        Recoil_theta;
        Double_t        Parent_E;
        Double_t        Parent_P[3];
        TString         *Parent_Volume;
        Double_t        Truth_Pi;
        Double_t        Truth_Pf;
        Double_t        Truth_P[3];
        Double_t        Target_Recoil_E;
        Double_t        Target_Recoil_theta;
        Double_t        Target_Recoil_pT;
        Double_t        trans_sep;
        Double_t        Decay_X;
        Double_t        Decay_Y;
        Double_t        Decay_Z;
        Int_t           Secondary_Found;
        Int_t           Secondary_PDG;
        Double_t        Secondary_MaxE;
        Double_t        Secondary_MaxE_P[3];
        TString         *Secondary_MaxE_PVName;
        TString         *Secondary_MaxE_Process;
        Int_t           DP_decay;
        Double_t        E_decay_1;
        Double_t        E_decay_2;
        Double_t        P_decay_1[3];
        Double_t        P_decay_2[3];
        Int_t           PDG_decay_1;
        Int_t           PDG_decay_2;
        TString         *MainProcessName;
        TString         *MainProcessPVName;
        Float_t         MainProcessEnergy;
        Float_t         MainProcessVertexZ;
        Int_t           process_HardBrem;
        Int_t           process_HardBrem_Target;
        Int_t           process_HardBrem_ECAL;
        Int_t           process_GMM;
        Int_t           process_GMM_Target;
        Int_t           process_GMM_ECAL;
        Int_t           process_EN;
        Int_t           process_EN_Target;
        Int_t           process_EN_ECAL;
        Int_t           process_PN;
        Int_t           process_PN_Target;
        Int_t           process_PN_ECAL;
        vector<int>     *process_ID;
        vector<TString> *process_PVName;
        vector<TVector3> *process_vertex;
        vector<float>   *process_energy;
        vector<int>     *process_parent_pdg;
        vector<int>     *process_parent_id;
        Int_t           TagTrk2_track_No;
        vector<double>  *TagTrk2_pp;
        vector<double>  *TagTrk2_track_chi2;
        vector<double>  *TagTrk2_track_chi2_algo;
        Int_t           RecTrk2_track_No;
        vector<double>  *RecTrk2_pp;
        vector<double>  *RecTrk2_fixed_pp;
        vector<double>  *RecTrk2_track_chi2;
        vector<double>  *RecTrk2_track_chi2_algo;
        vector<double>  *RecTrk2_vertex_z;
        vector<double>  *ECal_seed_x_truth;
        vector<double>  *ECal_seed_y_truth;
        vector<double>  *ECal_seed_px_truth;
        vector<double>  *ECal_seed_py_truth;
        vector<double>  *ECal_seed_pz_truth;
        vector<double>  *ECal_seed_e_truth;
        vector<double>  *ECal_seed_theta_truth;
        vector<double>  *ECal_seed_phi_truth;
        vector<int>     *ECal_seed_pdg;
        vector<int>     *ECal_seed_id_rec_track;
        vector<double>  *ECal_seed_x;
        vector<double>  *ECal_seed_y;
        vector<double>  *ECal_seed_px;
        vector<double>  *ECal_seed_py;
        vector<double>  *ECal_seed_pz;
        Int_t           ECAL_COL_SIZE;
        vector<double>  *ECAL_E_total;
        vector<double>  *ECAL_E_max;
        vector<double>  *ECAL_E_frac;
        vector<double>  *ECAL_Moment_Lat;
        vector<vector<double> > *ECAL_E_frac_vec;
        vector<vector<double> > *ECAL_Moment_R;
        vector<vector<double> > *ECAL_Moment_X;
        vector<vector<double> > *ECAL_Moment_Y;
        vector<vector<double> > *ECAL_Moment_Z;
        vector<double>  *ECAL_ClusterSub_E;
        vector<double>  *ECAL_ClusterSub_X;
        vector<double>  *ECAL_ClusterSub_Y;
        vector<double>  *ECAL_ClusterSub_Z;
        vector<double>  *ECAL_ClusterSub_Width_X;
        vector<double>  *ECAL_ClusterSub_Width_Y;
        vector<double>  *ECAL_ClusterSub_Width_Z;
        vector<int>     *ECAL_ClusterSub_NCell;
        vector<int>     *ECAL_ClusterSub_P0;
        vector<int>     *ECAL_ClusterSub_P1;
        vector<double>  *ECAL_ClusterSub_cosTheta;
        vector<double>  *ECAL_ClusterSub_phi;
        vector<double>  *ECAL_ClusterSub_X_cast;
        vector<double>  *ECAL_ClusterSub_Y_cast;
        Int_t           ECAL_ClusterSub_NCell_total;
        Int_t           ECAL_ClusterSub_N;
        Double_t        ECAL_ClusterSub_E_total;
        vector<int>     *ECAL_ClusterSub_matchRecTrk;
        vector<double>  *ECAL_Cluster_E;
        vector<double>  *ECAL_Cluster_X;
        vector<double>  *ECAL_Cluster_Y;
        vector<double>  *ECAL_Cluster_Z;
        vector<double>  *ECAL_Cluster_Width_X;
        vector<double>  *ECAL_Cluster_Width_Y;
        vector<double>  *ECAL_Cluster_Width_Z;
        vector<int>     *ECAL_Cluster_NCell;
        vector<int>     *ECAL_Cluster_NSub;
        vector<int>     *ECAL_Cluster_P0;
        vector<double>  *ECAL_Cluster_cosTheta;
        vector<double>  *ECAL_Cluster_phi;
        vector<double>  *ECAL_Cluster_X_cast;
        vector<double>  *ECAL_Cluster_Y_cast;
        Int_t           ECAL_Cluster_NCell_total;
        Int_t           ECAL_Cluster_N;
        Double_t        ECAL_Cluster_E_total;
        vector<int>     *ECAL_Cluster_NSub_orig;
        vector<int>     *ECAL_Cluster_NMatch_orig;
        vector<int>     *ECAL_NCell_max_XY;
        vector<int>     *ECAL_NCell_XY;
        Int_t           HCAL_COL_SIZE;
        vector<double>  *HCAL_E_total;
        vector<double>  *HCAL_E_Max_Cell;
        Int_t           SideHCAL_COL_SIZE;
        vector<double>  *SideHCAL_E_total;
        vector<double>  *SideHCAL_E_Max_Cell;
        vector<double>  *HCAL_E_Cali;
        vector<double>  *test_store;

        // List of branches
        TBranch        *b_RunNumber;   //!
        TBranch        *b_EventNumber;   //!
        TBranch        *b_Rndm;   //!
        TBranch        *b_weight;   //!
        TBranch        *b_ECAL_Leak;   //!
        TBranch        *b_PNEnergy_Target;   //!
        TBranch        *b_PNEnergy_ECAL;   //!
        TBranch        *b_ENEnergy_Target;   //!
        TBranch        *b_ENEnergy_ECAL;   //!
        TBranch        *b_PNZ_Target;   //!
        TBranch        *b_PNZ_ECAL;   //!
        TBranch        *b_ENZ_Target;   //!
        TBranch        *b_ENZ_ECAL;   //!
        TBranch        *b_Digitized_Signal_No;   //!
        TBranch        *b_Digitized_Signal_NoGen;   //!
        TBranch        *b_Initial_Px;   //!
        TBranch        *b_Initial_Py;   //!
        TBranch        *b_Initial_Pz;   //!
        TBranch        *b_Initial_X;   //!
        TBranch        *b_Initial_Y;   //!
        TBranch        *b_Initial_Z;   //!
        TBranch        *b_Recoil_E;   //!
        TBranch        *b_Recoil_P;   //!
        TBranch        *b_Recoil_pT;   //!
        TBranch        *b_Recoil_theta;   //!
        TBranch        *b_Parent_E;   //!
        TBranch        *b_Parent_P;   //!
        TBranch        *b_Parent_Volume;   //!
        TBranch        *b_Truth_Pi;   //!
        TBranch        *b_Truth_Pf;   //!
        TBranch        *b_Truth_P;   //!
        TBranch        *b_Target_Recoil_E;   //!
        TBranch        *b_Target_Recoil_theta;   //!
        TBranch        *b_Target_Recoil_pT;   //!
        TBranch        *b_trans_sep;   //!
        TBranch        *b_Decay_X;   //!
        TBranch        *b_Decay_Y;   //!
        TBranch        *b_Decay_Z;   //!
        TBranch        *b_Secondary_Found;   //!
        TBranch        *b_Secondary_PDG;   //!
        TBranch        *b_Secondary_MaxE;   //!
        TBranch        *b_Secondary_MaxE_P;   //!
        TBranch        *b_Secondary_MaxE_PVName;   //!
        TBranch        *b_Secondary_MaxE_Process;   //!
        TBranch        *b_DP_decay;   //!
        TBranch        *b_E_decay_1;   //!
        TBranch        *b_E_decay_2;   //!
        TBranch        *b_P_decay_1;   //!
        TBranch        *b_P_decay_2;   //!
        TBranch        *b_PDG_decay_1;   //!
        TBranch        *b_PDG_decay_2;   //!
        TBranch        *b_MainProcessName;   //!
        TBranch        *b_MainProcessPVName;   //!
        TBranch        *b_MainProcessEnergy;   //!
        TBranch        *b_MainProcessVertexZ;   //!
        TBranch        *b_process_HardBrem;   //!
        TBranch        *b_process_HardBrem_Target;   //!
        TBranch        *b_process_HardBrem_ECAL;   //!
        TBranch        *b_process_GMM;   //!
        TBranch        *b_process_GMM_Target;   //!
        TBranch        *b_process_GMM_ECAL;   //!
        TBranch        *b_process_EN;   //!
        TBranch        *b_process_EN_Target;   //!
        TBranch        *b_process_EN_ECAL;   //!
        TBranch        *b_process_PN;   //!
        TBranch        *b_process_PN_Target;   //!
        TBranch        *b_process_PN_ECAL;   //!
        TBranch        *b_process_ID;   //!
        TBranch        *b_process_PVName;   //!
        TBranch        *b_process_vertex;   //!
        TBranch        *b_process_energy;   //!
        TBranch        *b_process_parent_pdg;   //!
        TBranch        *b_process_parent_id;   //!
        TBranch        *b_TagTrk2_track_No;   //!
        TBranch        *b_TagTrk2_pp;   //!
        TBranch        *b_TagTrk2_track_chi2;   //!
        TBranch        *b_TagTrk2_track_chi2_algo;   //!
        TBranch        *b_RecTrk2_track_No;   //!
        TBranch        *b_RecTrk2_pp;   //!
        TBranch        *b_RecTrk2_fixed_pp;   //!
        TBranch        *b_RecTrk2_track_chi2;   //!
        TBranch        *b_RecTrk2_track_chi2_algo;   //!
        TBranch        *b_RecTrk2_vertex_z;   //!
        TBranch        *b_ECal_seed_x_truth;   //!
        TBranch        *b_ECal_seed_y_truth;   //!
        TBranch        *b_ECal_seed_px_truth;   //!
        TBranch        *b_ECal_seed_py_truth;   //!
        TBranch        *b_ECal_seed_pz_truth;   //!
        TBranch        *b_ECal_seed_e_truth;   //!
        TBranch        *b_ECal_seed_theta_truth;   //!
        TBranch        *b_ECal_seed_phi_truth;   //!
        TBranch        *b_ECal_seed_pdg;   //!
        TBranch        *b_ECal_seed_id_rec_track;   //!
        TBranch        *b_ECal_seed_x;   //!
        TBranch        *b_ECal_seed_y;   //!
        TBranch        *b_ECal_seed_px;   //!
        TBranch        *b_ECal_seed_py;   //!
        TBranch        *b_ECal_seed_pz;   //!
        TBranch        *b_ECAL_COL_SIZE;   //!
        TBranch        *b_ECAL_E_total;   //!
        TBranch        *b_ECAL_E_max;   //!
        TBranch        *b_ECAL_E_frac;   //!
        TBranch        *b_ECAL_Moment_Lat;   //!
        TBranch        *b_ECAL_E_frac_vec;   //!
        TBranch        *b_ECAL_Moment_R;   //!
        TBranch        *b_ECAL_Moment_X;   //!
        TBranch        *b_ECAL_Moment_Y;   //!
        TBranch        *b_ECAL_Moment_Z;   //!
        TBranch        *b_ECAL_ClusterSub_E;   //!
        TBranch        *b_ECAL_ClusterSub_X;   //!
        TBranch        *b_ECAL_ClusterSub_Y;   //!
        TBranch        *b_ECAL_ClusterSub_Z;   //!
        TBranch        *b_ECAL_ClusterSub_Width_X;   //!
        TBranch        *b_ECAL_ClusterSub_Width_Y;   //!
        TBranch        *b_ECAL_ClusterSub_Width_Z;   //!
        TBranch        *b_ECAL_ClusterSub_NCell;   //!
        TBranch        *b_ECAL_ClusterSub_P0;   //!
        TBranch        *b_ECAL_ClusterSub_P1;   //!
        TBranch        *b_ECAL_ClusterSub_cosTheta;   //!
        TBranch        *b_ECAL_ClusterSub_phi;   //!
        TBranch        *b_ECAL_ClusterSub_X_cast;   //!
        TBranch        *b_ECAL_ClusterSub_Y_cast;   //!
        TBranch        *b_ECAL_ClusterSub_NCell_total;   //!
        TBranch        *b_ECAL_ClusterSub_N;   //!
        TBranch        *b_ECAL_ClusterSub_E_total;   //!
        TBranch        *b_ECAL_ClusterSub_matchRecTrk;   //!
        TBranch        *b_ECAL_Cluster_E;   //!
        TBranch        *b_ECAL_Cluster_X;   //!
        TBranch        *b_ECAL_Cluster_Y;   //!
        TBranch        *b_ECAL_Cluster_Z;   //!
        TBranch        *b_ECAL_Cluster_Width_X;   //!
        TBranch        *b_ECAL_Cluster_Width_Y;   //!
        TBranch        *b_ECAL_Cluster_Width_Z;   //!
        TBranch        *b_ECAL_Cluster_NCell;   //!
        TBranch        *b_ECAL_Cluster_NSub;   //!
        TBranch        *b_ECAL_Cluster_P0;   //!
        TBranch        *b_ECAL_Cluster_cosTheta;   //!
        TBranch        *b_ECAL_Cluster_phi;   //!
        TBranch        *b_ECAL_Cluster_X_cast;   //!
        TBranch        *b_ECAL_Cluster_Y_cast;   //!
        TBranch        *b_ECAL_Cluster_NCell_total;   //!
        TBranch        *b_ECAL_Cluster_N;   //!
        TBranch        *b_ECAL_Cluster_E_total;   //!
        TBranch        *b_ECAL_Cluster_NSub_orig;   //!
        TBranch        *b_ECAL_Cluster_NMatch_orig;   //!
        TBranch        *b_ECAL_NCell_max_XY;   //!
        TBranch        *b_ECAL_NCell_XY;   //!
        TBranch        *b_HCAL_COL_SIZE;   //!
        TBranch        *b_HCAL_E_total;   //!
        TBranch        *b_HCAL_E_Max_Cell;   //!
        TBranch        *b_SideHCAL_COL_SIZE;   //!
        TBranch        *b_SideHCAL_E_total;   //!
        TBranch        *b_SideHCAL_E_Max_Cell;   //!
        TBranch        *b_HCAL_E_Cali;   //!
        TBranch        *b_test_store;   //!

        dp(TTree *tree=0);
        virtual ~dp();
        virtual Int_t    Cut(Long64_t entry);
        virtual Int_t    GetEntry(Long64_t entry);
        virtual Long64_t LoadTree(Long64_t entry);
        virtual void     Init(TTree *tree);
        virtual void     Loop();
        virtual Bool_t   Notify();
        virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef dp_cxx
dp::dp(TTree *tree) : fChain(0) 
{
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("dp_ana.root");
        if (!f || !f->IsOpen()) {
            f = new TFile("dp_ana.root");
        }
        f->GetObject("dp",tree);

    }
    Init(tree);
}

dp::~dp()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t dp::GetEntry(Long64_t entry)
{
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t dp::LoadTree(Long64_t entry)
{
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void dp::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).

    // Set object pointer
    Parent_Volume = 0;
    Secondary_MaxE_PVName = 0;
    Secondary_MaxE_Process = 0;
    MainProcessName = 0;
    MainProcessPVName = 0;
    process_ID = 0;
    process_PVName = 0;
    process_vertex = 0;
    process_energy = 0;
    process_parent_pdg = 0;
    process_parent_id = 0;
    TagTrk2_pp = 0;
    TagTrk2_track_chi2 = 0;
    TagTrk2_track_chi2_algo = 0;
    RecTrk2_pp = 0;
    RecTrk2_fixed_pp = 0;
    RecTrk2_track_chi2 = 0;
    RecTrk2_track_chi2_algo = 0;
    RecTrk2_vertex_z = 0;
    ECal_seed_x_truth = 0;
    ECal_seed_y_truth = 0;
    ECal_seed_px_truth = 0;
    ECal_seed_py_truth = 0;
    ECal_seed_pz_truth = 0;
    ECal_seed_e_truth = 0;
    ECal_seed_theta_truth = 0;
    ECal_seed_phi_truth = 0;
    ECal_seed_pdg = 0;
    ECal_seed_id_rec_track = 0;
    ECal_seed_x = 0;
    ECal_seed_y = 0;
    ECal_seed_px = 0;
    ECal_seed_py = 0;
    ECal_seed_pz = 0;
    ECAL_E_total = 0;
    ECAL_E_max = 0;
    ECAL_E_frac = 0;
    ECAL_Moment_Lat = 0;
    ECAL_E_frac_vec = 0;
    ECAL_Moment_R = 0;
    ECAL_Moment_X = 0;
    ECAL_Moment_Y = 0;
    ECAL_Moment_Z = 0;
    ECAL_ClusterSub_E = 0;
    ECAL_ClusterSub_X = 0;
    ECAL_ClusterSub_Y = 0;
    ECAL_ClusterSub_Z = 0;
    ECAL_ClusterSub_Width_X = 0;
    ECAL_ClusterSub_Width_Y = 0;
    ECAL_ClusterSub_Width_Z = 0;
    ECAL_ClusterSub_NCell = 0;
    ECAL_ClusterSub_P0 = 0;
    ECAL_ClusterSub_P1 = 0;
    ECAL_ClusterSub_cosTheta = 0;
    ECAL_ClusterSub_phi = 0;
    ECAL_ClusterSub_X_cast = 0;
    ECAL_ClusterSub_Y_cast = 0;
    ECAL_ClusterSub_matchRecTrk = 0;
    ECAL_Cluster_E = 0;
    ECAL_Cluster_X = 0;
    ECAL_Cluster_Y = 0;
    ECAL_Cluster_Z = 0;
    ECAL_Cluster_Width_X = 0;
    ECAL_Cluster_Width_Y = 0;
    ECAL_Cluster_Width_Z = 0;
    ECAL_Cluster_NCell = 0;
    ECAL_Cluster_NSub = 0;
    ECAL_Cluster_P0 = 0;
    ECAL_Cluster_cosTheta = 0;
    ECAL_Cluster_phi = 0;
    ECAL_Cluster_X_cast = 0;
    ECAL_Cluster_Y_cast = 0;
    ECAL_Cluster_NSub_orig = 0;
    ECAL_Cluster_NMatch_orig = 0;
    ECAL_NCell_max_XY = 0;
    ECAL_NCell_XY = 0;
    HCAL_E_total = 0;
    HCAL_E_Max_Cell = 0;
    SideHCAL_E_total = 0;
    SideHCAL_E_Max_Cell = 0;
    HCAL_E_Cali = 0;
    test_store = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
    fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
    fChain->SetBranchAddress("Rndm", Rndm, &b_Rndm);
    fChain->SetBranchAddress("weight", &weight, &b_weight);
    fChain->SetBranchAddress("ECAL_Leak", &ECAL_Leak, &b_ECAL_Leak);
    fChain->SetBranchAddress("PNEnergy_Target", &PNEnergy_Target, &b_PNEnergy_Target);
    fChain->SetBranchAddress("PNEnergy_ECAL", &PNEnergy_ECAL, &b_PNEnergy_ECAL);
    fChain->SetBranchAddress("ENEnergy_Target", &ENEnergy_Target, &b_ENEnergy_Target);
    fChain->SetBranchAddress("ENEnergy_ECAL", &ENEnergy_ECAL, &b_ENEnergy_ECAL);
    fChain->SetBranchAddress("PNZ_Target", &PNZ_Target, &b_PNZ_Target);
    fChain->SetBranchAddress("PNZ_ECAL", &PNZ_ECAL, &b_PNZ_ECAL);
    fChain->SetBranchAddress("ENZ_Target", &ENZ_Target, &b_ENZ_Target);
    fChain->SetBranchAddress("ENZ_ECAL", &ENZ_ECAL, &b_ENZ_ECAL);
    fChain->SetBranchAddress("Digitized_Signal_No", &Digitized_Signal_No, &b_Digitized_Signal_No);
    fChain->SetBranchAddress("Digitized_Signal_NoGen", &Digitized_Signal_NoGen, &b_Digitized_Signal_NoGen);
    fChain->SetBranchAddress("Initial_Px", &Initial_Px, &b_Initial_Px);
    fChain->SetBranchAddress("Initial_Py", &Initial_Py, &b_Initial_Py);
    fChain->SetBranchAddress("Initial_Pz", &Initial_Pz, &b_Initial_Pz);
    fChain->SetBranchAddress("Initial_X", &Initial_X, &b_Initial_X);
    fChain->SetBranchAddress("Initial_Y", &Initial_Y, &b_Initial_Y);
    fChain->SetBranchAddress("Initial_Z", &Initial_Z, &b_Initial_Z);
    fChain->SetBranchAddress("Recoil_E", &Recoil_E, &b_Recoil_E);
    fChain->SetBranchAddress("Recoil_P", Recoil_P, &b_Recoil_P);
    fChain->SetBranchAddress("Recoil_pT", &Recoil_pT, &b_Recoil_pT);
    fChain->SetBranchAddress("Recoil_theta", &Recoil_theta, &b_Recoil_theta);
    fChain->SetBranchAddress("Parent_E", &Parent_E, &b_Parent_E);
    fChain->SetBranchAddress("Parent_P", Parent_P, &b_Parent_P);
    fChain->SetBranchAddress("Parent_Volume", &Parent_Volume, &b_Parent_Volume);
    fChain->SetBranchAddress("Truth_Pi", &Truth_Pi, &b_Truth_Pi);
    fChain->SetBranchAddress("Truth_Pf", &Truth_Pf, &b_Truth_Pf);
    fChain->SetBranchAddress("Truth_P", Truth_P, &b_Truth_P);
    fChain->SetBranchAddress("Target_Recoil_E", &Target_Recoil_E, &b_Target_Recoil_E);
    fChain->SetBranchAddress("Target_Recoil_theta", &Target_Recoil_theta, &b_Target_Recoil_theta);
    fChain->SetBranchAddress("Target_Recoil_pT", &Target_Recoil_pT, &b_Target_Recoil_pT);
    fChain->SetBranchAddress("trans_sep", &trans_sep, &b_trans_sep);
    fChain->SetBranchAddress("Decay_X", &Decay_X, &b_Decay_X);
    fChain->SetBranchAddress("Decay_Y", &Decay_Y, &b_Decay_Y);
    fChain->SetBranchAddress("Decay_Z", &Decay_Z, &b_Decay_Z);
    fChain->SetBranchAddress("Secondary_Found", &Secondary_Found, &b_Secondary_Found);
    fChain->SetBranchAddress("Secondary_PDG", &Secondary_PDG, &b_Secondary_PDG);
    fChain->SetBranchAddress("Secondary_MaxE", &Secondary_MaxE, &b_Secondary_MaxE);
    fChain->SetBranchAddress("Secondary_MaxE_P", Secondary_MaxE_P, &b_Secondary_MaxE_P);
    fChain->SetBranchAddress("Secondary_MaxE_PVName", &Secondary_MaxE_PVName, &b_Secondary_MaxE_PVName);
    fChain->SetBranchAddress("Secondary_MaxE_Process", &Secondary_MaxE_Process, &b_Secondary_MaxE_Process);
    fChain->SetBranchAddress("DP_decay", &DP_decay, &b_DP_decay);
    fChain->SetBranchAddress("E_decay_1", &E_decay_1, &b_E_decay_1);
    fChain->SetBranchAddress("E_decay_2", &E_decay_2, &b_E_decay_2);
    fChain->SetBranchAddress("P_decay_1", P_decay_1, &b_P_decay_1);
    fChain->SetBranchAddress("P_decay_2", P_decay_2, &b_P_decay_2);
    fChain->SetBranchAddress("PDG_decay_1", &PDG_decay_1, &b_PDG_decay_1);
    fChain->SetBranchAddress("PDG_decay_2", &PDG_decay_2, &b_PDG_decay_2);
    fChain->SetBranchAddress("MainProcessName", &MainProcessName, &b_MainProcessName);
    fChain->SetBranchAddress("MainProcessPVName", &MainProcessPVName, &b_MainProcessPVName);
    fChain->SetBranchAddress("MainProcessEnergy", &MainProcessEnergy, &b_MainProcessEnergy);
    fChain->SetBranchAddress("MainProcessVertexZ", &MainProcessVertexZ, &b_MainProcessVertexZ);
    fChain->SetBranchAddress("process_HardBrem", &process_HardBrem, &b_process_HardBrem);
    fChain->SetBranchAddress("process_HardBrem_Target", &process_HardBrem_Target, &b_process_HardBrem_Target);
    fChain->SetBranchAddress("process_HardBrem_ECAL", &process_HardBrem_ECAL, &b_process_HardBrem_ECAL);
    fChain->SetBranchAddress("process_GMM", &process_GMM, &b_process_GMM);
    fChain->SetBranchAddress("process_GMM_Target", &process_GMM_Target, &b_process_GMM_Target);
    fChain->SetBranchAddress("process_GMM_ECAL", &process_GMM_ECAL, &b_process_GMM_ECAL);
    fChain->SetBranchAddress("process_EN", &process_EN, &b_process_EN);
    fChain->SetBranchAddress("process_EN_Target", &process_EN_Target, &b_process_EN_Target);
    fChain->SetBranchAddress("process_EN_ECAL", &process_EN_ECAL, &b_process_EN_ECAL);
    fChain->SetBranchAddress("process_PN", &process_PN, &b_process_PN);
    fChain->SetBranchAddress("process_PN_Target", &process_PN_Target, &b_process_PN_Target);
    fChain->SetBranchAddress("process_PN_ECAL", &process_PN_ECAL, &b_process_PN_ECAL);
    fChain->SetBranchAddress("process_ID", &process_ID, &b_process_ID);
    fChain->SetBranchAddress("process_PVName", &process_PVName, &b_process_PVName);
    fChain->SetBranchAddress("process_vertex", &process_vertex, &b_process_vertex);
    fChain->SetBranchAddress("process_energy", &process_energy, &b_process_energy);
    fChain->SetBranchAddress("process_parent_pdg", &process_parent_pdg, &b_process_parent_pdg);
    fChain->SetBranchAddress("process_parent_id", &process_parent_id, &b_process_parent_id);
    fChain->SetBranchAddress("TagTrk2_track_No", &TagTrk2_track_No, &b_TagTrk2_track_No);
    fChain->SetBranchAddress("TagTrk2_pp", &TagTrk2_pp, &b_TagTrk2_pp);
    fChain->SetBranchAddress("TagTrk2_track_chi2", &TagTrk2_track_chi2, &b_TagTrk2_track_chi2);
    fChain->SetBranchAddress("TagTrk2_track_chi2_algo", &TagTrk2_track_chi2_algo, &b_TagTrk2_track_chi2_algo);
    fChain->SetBranchAddress("RecTrk2_track_No", &RecTrk2_track_No, &b_RecTrk2_track_No);
    fChain->SetBranchAddress("RecTrk2_pp", &RecTrk2_pp, &b_RecTrk2_pp);
    fChain->SetBranchAddress("RecTrk2_fixed_pp", &RecTrk2_fixed_pp, &b_RecTrk2_fixed_pp);
    fChain->SetBranchAddress("RecTrk2_track_chi2", &RecTrk2_track_chi2, &b_RecTrk2_track_chi2);
    fChain->SetBranchAddress("RecTrk2_track_chi2_algo", &RecTrk2_track_chi2_algo, &b_RecTrk2_track_chi2_algo);
    fChain->SetBranchAddress("RecTrk2_vertex_z", &RecTrk2_vertex_z, &b_RecTrk2_vertex_z);
    fChain->SetBranchAddress("ECal_seed_x_truth", &ECal_seed_x_truth, &b_ECal_seed_x_truth);
    fChain->SetBranchAddress("ECal_seed_y_truth", &ECal_seed_y_truth, &b_ECal_seed_y_truth);
    fChain->SetBranchAddress("ECal_seed_px_truth", &ECal_seed_px_truth, &b_ECal_seed_px_truth);
    fChain->SetBranchAddress("ECal_seed_py_truth", &ECal_seed_py_truth, &b_ECal_seed_py_truth);
    fChain->SetBranchAddress("ECal_seed_pz_truth", &ECal_seed_pz_truth, &b_ECal_seed_pz_truth);
    fChain->SetBranchAddress("ECal_seed_e_truth", &ECal_seed_e_truth, &b_ECal_seed_e_truth);
    fChain->SetBranchAddress("ECal_seed_theta_truth", &ECal_seed_theta_truth, &b_ECal_seed_theta_truth);
    fChain->SetBranchAddress("ECal_seed_phi_truth", &ECal_seed_phi_truth, &b_ECal_seed_phi_truth);
    fChain->SetBranchAddress("ECal_seed_pdg", &ECal_seed_pdg, &b_ECal_seed_pdg);
    fChain->SetBranchAddress("ECal_seed_id_rec_track", &ECal_seed_id_rec_track, &b_ECal_seed_id_rec_track);
    fChain->SetBranchAddress("ECal_seed_x", &ECal_seed_x, &b_ECal_seed_x);
    fChain->SetBranchAddress("ECal_seed_y", &ECal_seed_y, &b_ECal_seed_y);
    fChain->SetBranchAddress("ECal_seed_px", &ECal_seed_px, &b_ECal_seed_px);
    fChain->SetBranchAddress("ECal_seed_py", &ECal_seed_py, &b_ECal_seed_py);
    fChain->SetBranchAddress("ECal_seed_pz", &ECal_seed_pz, &b_ECal_seed_pz);
    fChain->SetBranchAddress("ECAL_COL_SIZE", &ECAL_COL_SIZE, &b_ECAL_COL_SIZE);
    fChain->SetBranchAddress("ECAL_E_total", &ECAL_E_total, &b_ECAL_E_total);
    fChain->SetBranchAddress("ECAL_E_max", &ECAL_E_max, &b_ECAL_E_max);
    fChain->SetBranchAddress("ECAL_E_frac", &ECAL_E_frac, &b_ECAL_E_frac);
    fChain->SetBranchAddress("ECAL_Moment_Lat", &ECAL_Moment_Lat, &b_ECAL_Moment_Lat);
    fChain->SetBranchAddress("ECAL_E_frac_vec", &ECAL_E_frac_vec, &b_ECAL_E_frac_vec);
    fChain->SetBranchAddress("ECAL_Moment_R", &ECAL_Moment_R, &b_ECAL_Moment_R);
    fChain->SetBranchAddress("ECAL_Moment_X", &ECAL_Moment_X, &b_ECAL_Moment_X);
    fChain->SetBranchAddress("ECAL_Moment_Y", &ECAL_Moment_Y, &b_ECAL_Moment_Y);
    fChain->SetBranchAddress("ECAL_Moment_Z", &ECAL_Moment_Z, &b_ECAL_Moment_Z);
    fChain->SetBranchAddress("ECAL_ClusterSub_E", &ECAL_ClusterSub_E, &b_ECAL_ClusterSub_E);
    fChain->SetBranchAddress("ECAL_ClusterSub_X", &ECAL_ClusterSub_X, &b_ECAL_ClusterSub_X);
    fChain->SetBranchAddress("ECAL_ClusterSub_Y", &ECAL_ClusterSub_Y, &b_ECAL_ClusterSub_Y);
    fChain->SetBranchAddress("ECAL_ClusterSub_Z", &ECAL_ClusterSub_Z, &b_ECAL_ClusterSub_Z);
    fChain->SetBranchAddress("ECAL_ClusterSub_Width_X", &ECAL_ClusterSub_Width_X, &b_ECAL_ClusterSub_Width_X);
    fChain->SetBranchAddress("ECAL_ClusterSub_Width_Y", &ECAL_ClusterSub_Width_Y, &b_ECAL_ClusterSub_Width_Y);
    fChain->SetBranchAddress("ECAL_ClusterSub_Width_Z", &ECAL_ClusterSub_Width_Z, &b_ECAL_ClusterSub_Width_Z);
    fChain->SetBranchAddress("ECAL_ClusterSub_NCell", &ECAL_ClusterSub_NCell, &b_ECAL_ClusterSub_NCell);
    fChain->SetBranchAddress("ECAL_ClusterSub_P0", &ECAL_ClusterSub_P0, &b_ECAL_ClusterSub_P0);
    fChain->SetBranchAddress("ECAL_ClusterSub_P1", &ECAL_ClusterSub_P1, &b_ECAL_ClusterSub_P1);
    fChain->SetBranchAddress("ECAL_ClusterSub_cosTheta", &ECAL_ClusterSub_cosTheta, &b_ECAL_ClusterSub_cosTheta);
    fChain->SetBranchAddress("ECAL_ClusterSub_phi", &ECAL_ClusterSub_phi, &b_ECAL_ClusterSub_phi);
    fChain->SetBranchAddress("ECAL_ClusterSub_X_cast", &ECAL_ClusterSub_X_cast, &b_ECAL_ClusterSub_X_cast);
    fChain->SetBranchAddress("ECAL_ClusterSub_Y_cast", &ECAL_ClusterSub_Y_cast, &b_ECAL_ClusterSub_Y_cast);
    fChain->SetBranchAddress("ECAL_ClusterSub_NCell_total", &ECAL_ClusterSub_NCell_total, &b_ECAL_ClusterSub_NCell_total);
    fChain->SetBranchAddress("ECAL_ClusterSub_N", &ECAL_ClusterSub_N, &b_ECAL_ClusterSub_N);
    fChain->SetBranchAddress("ECAL_ClusterSub_E_total", &ECAL_ClusterSub_E_total, &b_ECAL_ClusterSub_E_total);
    fChain->SetBranchAddress("ECAL_ClusterSub_matchRecTrk", &ECAL_ClusterSub_matchRecTrk, &b_ECAL_ClusterSub_matchRecTrk);
    fChain->SetBranchAddress("ECAL_Cluster_E", &ECAL_Cluster_E, &b_ECAL_Cluster_E);
    fChain->SetBranchAddress("ECAL_Cluster_X", &ECAL_Cluster_X, &b_ECAL_Cluster_X);
    fChain->SetBranchAddress("ECAL_Cluster_Y", &ECAL_Cluster_Y, &b_ECAL_Cluster_Y);
    fChain->SetBranchAddress("ECAL_Cluster_Z", &ECAL_Cluster_Z, &b_ECAL_Cluster_Z);
    fChain->SetBranchAddress("ECAL_Cluster_Width_X", &ECAL_Cluster_Width_X, &b_ECAL_Cluster_Width_X);
    fChain->SetBranchAddress("ECAL_Cluster_Width_Y", &ECAL_Cluster_Width_Y, &b_ECAL_Cluster_Width_Y);
    fChain->SetBranchAddress("ECAL_Cluster_Width_Z", &ECAL_Cluster_Width_Z, &b_ECAL_Cluster_Width_Z);
    fChain->SetBranchAddress("ECAL_Cluster_NCell", &ECAL_Cluster_NCell, &b_ECAL_Cluster_NCell);
    fChain->SetBranchAddress("ECAL_Cluster_NSub", &ECAL_Cluster_NSub, &b_ECAL_Cluster_NSub);
    fChain->SetBranchAddress("ECAL_Cluster_P0", &ECAL_Cluster_P0, &b_ECAL_Cluster_P0);
    fChain->SetBranchAddress("ECAL_Cluster_cosTheta", &ECAL_Cluster_cosTheta, &b_ECAL_Cluster_cosTheta);
    fChain->SetBranchAddress("ECAL_Cluster_phi", &ECAL_Cluster_phi, &b_ECAL_Cluster_phi);
    fChain->SetBranchAddress("ECAL_Cluster_X_cast", &ECAL_Cluster_X_cast, &b_ECAL_Cluster_X_cast);
    fChain->SetBranchAddress("ECAL_Cluster_Y_cast", &ECAL_Cluster_Y_cast, &b_ECAL_Cluster_Y_cast);
    fChain->SetBranchAddress("ECAL_Cluster_NCell_total", &ECAL_Cluster_NCell_total, &b_ECAL_Cluster_NCell_total);
    fChain->SetBranchAddress("ECAL_Cluster_N", &ECAL_Cluster_N, &b_ECAL_Cluster_N);
    fChain->SetBranchAddress("ECAL_Cluster_E_total", &ECAL_Cluster_E_total, &b_ECAL_Cluster_E_total);
    fChain->SetBranchAddress("ECAL_Cluster_NSub_orig", &ECAL_Cluster_NSub_orig, &b_ECAL_Cluster_NSub_orig);
    fChain->SetBranchAddress("ECAL_Cluster_NMatch_orig", &ECAL_Cluster_NMatch_orig, &b_ECAL_Cluster_NMatch_orig);
    fChain->SetBranchAddress("ECAL_NCell_max_XY", &ECAL_NCell_max_XY, &b_ECAL_NCell_max_XY);
    fChain->SetBranchAddress("ECAL_NCell_XY", &ECAL_NCell_XY, &b_ECAL_NCell_XY);
    fChain->SetBranchAddress("HCAL_COL_SIZE", &HCAL_COL_SIZE, &b_HCAL_COL_SIZE);
    fChain->SetBranchAddress("HCAL_E_total", &HCAL_E_total, &b_HCAL_E_total);
    fChain->SetBranchAddress("HCAL_E_Max_Cell", &HCAL_E_Max_Cell, &b_HCAL_E_Max_Cell);
    fChain->SetBranchAddress("SideHCAL_COL_SIZE", &SideHCAL_COL_SIZE, &b_SideHCAL_COL_SIZE);
    fChain->SetBranchAddress("SideHCAL_E_total", &SideHCAL_E_total, &b_SideHCAL_E_total);
    fChain->SetBranchAddress("SideHCAL_E_Max_Cell", &SideHCAL_E_Max_Cell, &b_SideHCAL_E_Max_Cell);
    fChain->SetBranchAddress("HCAL_E_Cali", &HCAL_E_Cali, &b_HCAL_E_Cali);
    fChain->SetBranchAddress("test_store", &test_store, &b_test_store);
    Notify();
}

Bool_t dp::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.

    return kTRUE;
}

void dp::Show(Long64_t entry)
{
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t dp::Cut(Long64_t entry)
{
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}
#endif // #ifdef dp_cxx
