---
title: 配置Docker编程环境 - macOS
theme: uncover
paginate: true
author: Xuliang Zhu
header: 配置Docker编程环境 - macOS
footer: '[暗光子软件研讨会](https://indico-tdli.sjtu.edu.cn/event/2140/) 2024/01/19 '
---

<!-- _paginate: skip -->

<!-- _header: "" -->

# 配置Docker编程环境 - macOS

<div align="right">👉 <a href="https://2024-winter-software-tutorial-darkshine-knowledg-6537bc19186da3.gitlab.io/prereqs-win.html">Windows</a></div>

Xuliang Zhu

![width:150px](assets/DarkSHINE-logo.png?text=C)


---

- 用Docker可以确保我们的程序在**跨平台**时有一样的表现，避免“在我机器上能运行，但是在别的机器上就不行🤔”的问题。

🐳 🐳 🐳

1. 安装Docker
2. 安装X11服务端
3. 使用DarkSHINE Docker
4. 配置CLion + Docker 工具链

---

<div align="left"><b><big>安装Docker</big></b></div>

<div align="left">

📦 下载[Docker Desktop](https://www.docker.com/products/docker-desktop/)并按照安装说明操作。

<br>
🚀 Docker是创建→ 部署→ 运行应用程序的工具。

🍶 Docker使用容器来运行应用程序。容器就像一个轻量级的、独立的操作系统，可以在任何环境中运行。

⚡️ 容器利用宿主机的内核，比虚拟机更快速和高效。

</div>

---

<div style="font-size: 26px;">



<div align="left"><big><b>安装X11服务端</b></big></div>

1. 下载[Xquartz](https://www.xquartz.org)并根据提示安装，或```brew install xquartz```，二选一。

2. 打开Xquartz，点击屏幕左上角Xquartz | Settings | **Security**.

   勾选 Allow connections from network clients

   ![height:200px](assets/image-20240117145544330.png) ![height:200px](assets/image-20240118003119434.png)

3. 打开![width:32px](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Terminalicon2.png/240px-Terminalicon2.png)Terminal，输入命令以启用OpenGL渲染:
   ```defaults write org.xquartz.X11 enable_iglx -bool true```

4. 重启Xquartz

5. 每次启动电脑后，在终端输入`xhost +localhost`

</div>

---

<div style="font-size: 30px;">

<div align="left"><big><b><a href="https://hub.docker.com/r/ykrsama/darkshine-simulation">使用 DarkSHINE Docker</a></b></big></div>

1. 后台运行![width:32px](assets/image-20240117212841120.png)Docker，在![width:32px](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b3/Terminalicon2.png/240px-Terminalicon2.png)Terminal下载DarkSHINE Docker和脚本：

   ```bash
   docker pull ykrsama/darkshine-simulation
   git clone https://gitlab.com/ykrsama/darkshine-docker.git
   ```

3. 测试

   ```bash
   cd darkshine-docker
   ./docker-macos.sh
   DSimu -b 100 # 读 default.yaml, 输出 dp_simu.root
   DDis # 事例显示，读dp_simu.root
   exit
   ```

4. 添加一个DarkSHINE Docker启动命令

   ```bash
   echo alias dss-docker=\"$PWD/docker-macos.sh bash --login\" >> ~/.zshrc
   source ~/.zshrc
   ```
</div>

![bg right:33% 100%](assets/image-20240117194016789.png)

---

# CLion + Docker 工具链
---

## 免费安装CLion

- 申请[Github student pack](https://education.github.com/pack)
- [连接Github和JB账号](https://education.github.com/pack/offers#jetbrains)
- 安装 [JB Toolbox + CLion](https://www.jetbrains.com/toolbox-app/) 

---

1. 新建一个CLion项目（New Project），选择一个模版，例如C++ Executable

   ![width:1000px](assets/image-20240117183911717.png)

   ![width:1000px](assets/image-20240117184105849.png)

---

<div style="font-size: 30px;">


1. 到 Settings | Build, Execution, Deployment | **Toolchains**.

   点+，选择Docker

   ![width:380px](assets/docker-0001.png)

2. 设置Name, Server和Image

   ![width:850](assets/image-20240117171934222.png)
   

</div>

---

<div style="font-size: 24px;">

3. （可选）**Container Settings**，用于X11转发Docker图形界面

   Volume bindings: ```/tmp/.X11-unix``` , ```/tmp/.X11-unix```

   Environment variables: ```DISPLAY``` , ```host.docker.internal:0```

   Run options: ```--rm --net=host```
   
   ![width:450px](assets/image-20240117180757304.png)

</div>

---

<div style="font-size: 30px;">

4. 到 Settings | Build, Execution, Deployment | **CMake**.

   设置Toolchain为DarkSHINE Docker

   ![width:650px](assets/image-20240117184510562.png)

5. Docker内cmake版本为 3.16，需要修改模版里的```CMakeList.txt```第一行为：

   ```
   cmake_minimum_required(VERSION 3.16)
   ```

6. ![width:30px](assets/image-20240118141022426.png)Relaod CMake Project、 ![width:30px](assets/image-20240117191829188.png)编译、![width:30px](assets/image-20240117191907804.png)运行！

</div>

---

# *Fine*
