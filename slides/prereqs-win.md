---
title: 配置Docker编程环境 - Win10/11
theme: uncover
paginate: true
author: Xuliang Zhu
header: 配置Docker编程环境 - Win10/11
footer: '[暗光子软件研讨会](https://indico-tdli.sjtu.edu.cn/event/2140/) 2024/01/19 '
---

<!-- _paginate: skip -->

<!-- _header: "" -->

## 配置Docker编程环境 - Win 10/11

<div align="right">👉 <a href="https://2024-winter-software-tutorial-darkshine-knowledg-6537bc19186da3.gitlab.io/prereqs-mac.html">macOS</a></div>

Xuliang Zhu

![width:150px](assets/DarkSHINE-logo.png?text=C)


---

- 用Docker可以确保我们的程序在**跨平台**时有一样的表现，避免“在我机器上能运行，但是在别的机器上就不行🤔”的问题。

🐳 🐳 🐳

1. 安装WSL2+Docker
2. 安装X11服务端
3. 使用DarkSHINE Docker
4. 配置CLion + Docker 工具链

---

<div style="font-size:26px;">

<div align="left"><big><b>安装WSL2 + Docker</b></big></div>

- 在**管理员模式**下打开 PowerShell（右键单击并选择“以管理员身份运行”）

  ![width:400](assets/ps1.png)

- 输入```wsl --install```命令，若果输出help信息，说明已经安装过，可以跳过这一页。遇到任何问题，查看[排查安装问题](https://learn.microsoft.com/zh-cn/windows/wsl/troubleshooting)一文。

  ![width:400px](https://learn.microsoft.com/zh-cn/windows/wsl/media/wsl-install.png)

- 需要**重启**计算机。

</div>

---

<div style="font-size: 26px;">

1. 下载[Docker Desktop](https://www.docker.com/get-started/)并按照安装说明操作

2. 安装后，从 Windows 开始菜单启动 Docker Desktop，然后从任务栏的隐藏图标菜单中选择 Docker 图标。 右键单击该图标以显示 Docker 命令菜单，然后选择“设置”。
   ![width:800px](https://learn.microsoft.com/zh-cn/windows/wsl/media/docker-starting.png)
   

</div>

---

<div style="font-size: 26px;">

3. 确保在“设置”>“常规”中选中“使用基于 WSL 2 的引擎”。

   ![width:800](https://learn.microsoft.com/zh-cn/windows/wsl/media/docker-running.png)

</div>

---

<div style="font-size: 26px;">

<div align="left"><big><b>安装X11服务端</b></big></div>

- 下载[VcXsrv](https://sourceforge.net/projects/vcxsrv/)并根据提示安装。

- 安装结束后，打开xlaunch.exe，勾选 Disable access control ，其他选项保持默认（并允许XcvSrv通过防火墙）

   ![height:400px](https://rushichaudhari.github.io/img/2020-08-19-DockerGUIwsl/disableaccesscontrol.jpg) ![height:400px](https://rushichaudhari.github.io/img/2020-08-19-DockerGUIwsl/firewall.jpg)

</div>

---

<div style="font-size: 26px;">


<div align="left"><big><b><a href="https://hub.docker.com/r/ykrsama/darkshine-simulation">使用 DarkSHINE Docker</a></b></big></div>

1. 以**管理员**身份打开PowerShell输入
   <small>```set-executionpolicy remotesigned```</small> 并选择Y

   ![width:500px](assets/02ce611606e1496082f8edbed6e71b6a.png)

2. 后台运行![width:32px](assets/image-20240117212841120.png)Docker，打开Powershell，下载DarkSHINE Docker和脚本：

   ```bash
   docker pull ykrsama/darkshine-simulation
   git clone https://gitlab.com/ykrsama/darkshine-docker.git
   ```

3. 测试

   ```bash
   cd darkshine-docker
   ./docker-win.ps1
   DSimu -b 100 # 读 default.yaml, 输出 dp_simu.root
   DDis # 事例显示，读dp_simu.root
   exit
   ```

</div>

![bg right:33% 100%](assets/image-20240117194016789.png)

---

# CLion + Docker 工具链
---

## 免费安装CLion

- 申请[Github student pack](https://education.github.com/pack)
- [连接Github和JB账号](https://education.github.com/pack/offers#jetbrains)
- 安装 [JB Toolbox + CLion](https://www.jetbrains.com/toolbox-app/) 

---

1. 新建一个CLion项目（New Project），选择一个模版，例如C++ Executable

   ![width:1000px](assets/image-20240117183911717.png)

   ![width:1000px](assets/image-20240117184105849.png)

---

<div style="font-size: 30px;">

1. 到 Settings | Build, Execution, Deployment | **Toolchains**.

   点+，选择Docker

   ![width:380px](assets/docker-0001.png)

2. 设置Name, Server和Image

   ![width:850](assets/image-20240117171934222.png)

</div>

---

<div style="font-size: 24px;">


3. （可选）**Container Settings**，用于X11转发Docker图形界面

   Volume bindings: ```/tmp/.X11-unix``` , ```/tmp/.X11-unix```

   Environment variables: ```DISPLAY``` , ```host.docker.internal:0```

   Run options: ```--rm --net=host```
   
   ![width:450px](assets/image-20240117180757304.png)

</div>

---

<div style="font-size: 30px;">

4. 到 Settings | Build, Execution, Deployment | **CMake**.

   设置Toolchain为DarkSHINE Docker

   ![width:650px](assets/image-20240117184510562.png)

5. Docker内cmake版本为 3.16，需要修改模版里的```CMakeList.txt```第一行为：

   ```
   cmake_minimum_required(VERSION 3.16)
   ```

6. ![width:30px](assets/image-20240118141022426.png)Relaod CMake Project、![width:30px](assets/image-20240117191829188.png)编译、![width:30px](assets/image-20240117191907804.png)运行！

</div>

---

# *Fine*
