---
title: C++常用工具
theme: uncover
paginate: true
description: An example slide deck created by Marp CLI
author: Yuki Hattori
header: Day2 C++常用工具
footer: '[暗光子软件研讨会](https://indico-tdli.sjtu.edu.cn/event/2140/) 2024/01/20 '
---

<!-- _paginate: skip -->

# C++常用工具

Xuliang Zhu

![width:150px](assets/DarkSHINE-logo.png?text=C)

---

# 目录

- C++编辑器和IDE
- 版本控制
- 调试
- 动态分析

---

# C++编辑器和IDE

- **vim + YouCompleteMe插件**
- VSCode / **CLion**

---

#  GIT版本控制 

Git是一个版本控制系统，用于跟踪和协调计算机文件的变更，特别是在软件开发中。

#  ![bg left](assets/image-20240119163507095.png)

---

<div style="font-size: 26px;">
<big><b>配置git</b></big>

- 首先在本地创建ssh密钥对<small>（mac终端/git bash）</small>:

```bash
ssh-keygen -t rsa 
# Enter file in which to save the key: 直接回车
# 如果跳出Overwrite (y/n)? 即已经有了ssh密钥，选n，结束这一步。
# Enter passphrase：直接回车
cat ~/.ssh/id_rsa.pub
# 然后复制输出的内容，类似ssh-rsa AA....
```

- 回到github网页，进入头像| Settings，左边选择SSH and GPG Keys，New SSH key。Title随意，Key粘贴id_rsa.pub的内容。

![bg right:33% 100%](assets/img-gitssh.png)

- 验证是否添加成功：
  ```bash
  ssh -T git@github.com
  ```
  <small>如果是第一次的会提示是否continue，输入yes就会看到：You've successfully authenticated, but GitHub does not provide shell access 。这就表示已成功连上github。</small>

---

<div style="font-size: 26px;">


- 接下来我们要做的就是把本地仓库传到github上去，在此之前还需要设置username和email，因为github每次commit都会记录他们。

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```

- 在github主页，左上角![height:30px](assets/github-new.png)，新建一个仓库，设置Repository name（例如2024-playground)。不选添加额外README file和.gitignore。

![bg right:33% 100%](assets/image-20240120102354822.png)

- 添加一些文件

  ```
  mkdir playground && cd playground
  touch main.cpp
  ```

- 初始化git仓库并添加远程地址

```bash
git init . # 这一步会产生.git/
git remote add origin git@github.com:<github用户名>/<仓库名字>.git
```



</div>

---

<div style="font-size: 26px;">

<big><b>git工作流</b></big>

你的本地仓库由 git 维护的三棵"树"组成。第一个是你的 **工作目录**，它持有实际文件；第二个是**暂存区（Index）**，它像个缓存区域，临时保存你的改动；最后是Repository中的**HEAD**，它指向你最后一次提交的结果。

![width:900px](assets/bd67223a5540454c9235ddd98f79764c.png)

```bash
git add . # 把工作目录的改动加入Index
git commit # 把改动提交到HEAD。
           # 需要在弹出的文本编辑器中编辑代码提交信息。或git commit -m "代码提交信息"
git push # 把本地仓库的改动提交到远端仓库
         # 第一次需要 git push --set-upstream origin master
```

</div>

---

<div style="font-size: 26px;">
<big><b>分支</b></big>

分支是用来将特性开发绝缘开来的。在你创建仓库的时候，一般*master* 或*main*是默认的分支。在其他分支上进行开发，完成后再将它们合并到主分支上。

![bg right:33% 100%](https://i.stack.imgur.com/k5Gdu.png)

```bash
git status # 查看本地仓库所在分支和状态
git checkout -b feature_x # 创建一个新的分支
git checkout master # 切换回主分支
git branch -d feature_x # 删除新建的分支
git push origin <branch> # 推送分支到远端仓库
git pull # 更新本地仓库到最新
```

<big><b>更新与合并</b></big>

```
git status
git pull
git merge
git diff
```

</div>

---

# 调试

- 断点
- 监控表达式
- 内存视图
- [更多详见CLion文档](https://www.jetbrains.com/help/clion/debugging-code.html)

---

<div style="font-size: 26px;">

断点是暂停程序执行的标记。断点用于检查程序的状态。可以直接在代码中添加断电，或设置复杂的触发逻辑（例如条件判断、log消息）。

![width:700](assets/clion-breakpoint.png)

</div>

---

<div style="font-size: 26px;">

开始debug后可以计算复杂的表达式。

![width:700px](assets/clion-watch.png)

</div>

---

<div style="font-size: 26px;">

在某些情况下，例如在调试数据处理问题时，我们可能需要查看正在运行的进程的原始内存。可以从Variable选项卡中的指针跳转到包含所需地址的内存区域，并在单步执行程序的同时检查更改。

![width:800px](assets/clion-mem.png)

</div>

---



# 动态分析

- 内存错误检测 - Valgrind
- CPU性能分析 - Perf

---

初始设置

Perf executable: `/usr/bin/perf`

Valgrind executable: `/usr/bin/valgrind`

![width:500px](assets/perf.png) ![width:500px](assets/valgrind.png)

---

内存错误检测 - Valgrind

![width:500px](assets/image-20240120123622002.png)

---

CPU性能分析 - Perf

<div style="font-size: 26px;">

![width:500px](assets/flamechart-5725725.png)


y 轴表示调用栈，每一层都是一个函数。调用栈越深，火焰就越高，顶部就是正在执行的函数，下方都是它的母函数。

x 轴表示抽样数，如果一个函数在 x 轴占据的宽度越宽，就表示它被抽到的次数多，即执行的时间长。注意，x 轴不代表时间，而是所有的调用栈合并后，按字母顺序排列的。

</div>

---

# *Fine*

---
